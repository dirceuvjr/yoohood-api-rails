class Invitation < ApplicationRecord
  # belongs_to :user
  belongs_to :event

  belongs_to :inviter, :class_name => "User", :foreign_key => "inviter_id"
  belongs_to :invitee, :class_name => "User", :foreign_key => "invitee_id"
end
