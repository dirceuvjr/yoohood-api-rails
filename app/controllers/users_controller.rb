class UsersController < ApplicationController
  before_action :set_user, only: [
    :show, :update, :destroy, :create_user_events, :get_user_events,
    :delete_user_event, :get_referral
  ]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: {:user => @user}
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end
  
  # DELETE /users/1
  def destroy
    @user.destroy
  end
  
  def login
    @user = nil
    
    if (params[:email] == nil)
      @user = eval(ENV['PAYLOAD'])
      puts 'erro no login'
      puts @user
  
      @fb_object = {
        authenticated: {
          access: {
            expires_in: @user[:expiresIn],
            token: @user[:accessToken]
          },
          user: {
            id: @user[:id],
            name: @user[:name]
          }
        }
      }
    else
      if User.exists?(facebook_id: params[:facebook][:id])
        @user = User.where(facebook_id: params[:facebook][:id]).first
      else
        @user = User.create(
          :facebook_id => params[:facebook][:id],
          :name => params[:facebook][:name],
          :email => params[:facebook][:email],
          :country => 'Brasil',
          :event_range => 1000
        )
      end
  
      @fb_object = {
        authenticated: {
          access: {
            expires_in: params[:facebook][:expiresIn],
            token: params[:facebook][:accessToken]
          },
          user: {
            id: params[:facebook][:id],
            name: params[:facebook][:name]
          }
        }
      }
    end
    
    render json: @fb_object
  end

  def get_user_events
    query = [
      "start_time >= ? AND end_time <= ? AND event_type != ?",
      DateTime.now,
      DateTime.now + 90.days,
      ""
    ]
    @events = @user.events.where(query).order(:start_time => :asc)
    render json: { :user => { :pages => parse_event(@events) } }
  end
  
  def create_user_events
    page_id = params[:userPage][:page_id] || 0
    if page_id.to_i.digits.count > 4
      if (Event.exists?(:facebook_id => page_id) && !@user.events.exists?(:facebook_id => page_id))
        @event = Event.find_by(:facebook_id => page_id)
        @user.events.push(@event)
        @user.save!
        render json: parse_event(@user.events)
      else
        render json: {:status => 'Entry by facebook_id not found'}
      end
    else
      if (Event.exists?(:id => page_id) && !@user.events.exists?(:id => page_id))
        @event = Event.find_by(:id => page_id)
        @user.events.push(@event)
        @user.save!
        render json: parse_event(@user.events)
      else
        render json: {:status => 'Entry by id not found'}
      end
    end

  end
  
  def delete_user_event
    page_id = params[:event_id] || 0
    if page_id.to_i.digits.count > 4
      @event = Event.find_by(:facebook_id => page_id)
      @user.events.delete(@event)
      if @user.save
        render json: @user.events
      end
    else
      @event = Event.find_by(:id => page_id)
      @user.events.delete(@event)
      if @user.save
        render json: @user.events
      end
    end
    # if (Event.exists?(:facebook_id => page_id) && !@user.events.exists?(:facebook_id => page_id))
    # else
    #   render json: {:status => 'Entry not found'}
    # end

  end

  def get_referral
    @sent_invites = @user.sent_invites
    @referrals = []
    @sent_invites.each do |invite|
      @referrals.push(invite) if invite.accepted
    end

    render json: {:user => @referrals}
  end

  def get_referral_report
    @sent_invites = @user.sent_invites
    @referrals = []
    @sent_invites.each do |invite|
      @referrals.push(invite) if invite.accepted
    end

    render json: {:referral => @referrals}
  end

  def accept_invite
    # {"userPage":{"user_id":"1603154646416478","page_id":"1","referral_id":14}}
    inviter = User.find(params[:userPage][:user_id])
    invitee = User.find(params[:userPage][:referral_id])
    event = Event.find(params[:userPage][:page_id])

    @invitation = Invitation.new
    @invitation.inviter = inviter
    @invitation.invitee = invitee
    @invitation.event = event

    if @invitation.save
      render json: 200
    else
      render json: 500
    end
  end

  def get_inviters
    @inviters = User.all

    render json: @inviters
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      id = params[:id] || params[:facebook_id]
      if id.to_i.digits.count > 4
        set_user_by_fb_id
      else
        set_user_by_id
      end
    end

    def set_user_by_id
      @user = User.find(params[:id])
    end

    def set_user_by_fb_id
      @user = User.where(:facebook_id => params[:id]).first
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:facebook_id, :name, :email, :street, :number, :town, :city, :state, :country, :phone, :profile_picture, :event_range)
    end

    def parse_event(events)
      events.map do |event|
        {
          :id => event.id,
          :name => event.name,
          :facebook => event.facebook_id,
          :description => event.description,
          :start_at => event.start_time,
          :end_at => event.end_time,
          :cover => event.cover,
          :owner => {
            :id => event.owner_id,
            :name => event.owner_name
          },
          :type => event.event_type,
          :category => '',
          :ticket_uri => event.ticket_uri,
          :sponsored => event.sponsored,
          :city => event.city,
          :place => event.place,
          :location => {
            :longitude => event.longitude,
            :latitude => event.latitude
          }
        }
      end
    end
end
