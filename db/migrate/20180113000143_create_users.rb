class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.integer :facebook_id
      t.string :name
      t.string :email
      t.string :street
      t.string :number
      t.string :town
      t.string :city
      t.string :state
      t.string :country
      t.string :phone
      t.string :profile_picture
      t.integer :event_range

      t.timestamps
    end
  end
end
