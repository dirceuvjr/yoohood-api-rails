module Seed
  gastronomia = Category.new({ name: 'Gastronomia', active: true })
  gastronomia.save!
  danca = Category.new({ name: 'Dança', active: true })
  danca.save!
  festa = Category.new({ name: 'Festa', active: true })
  festa.save!
  
  Category.create([
    { name: 'Food truck', category: gastronomia, active: true },
    { name: 'Samba', category: danca, active: true },
    { name: 'Festa à fantasia', category: festa, active: true },
    { name: 'Rave', category: festa, active: true },
    { name: 'Folclore polonês', category: danca, active: true },
    { name: 'Culinária portuguesa', category: gastronomia, active: true },
    { name: 'Balada', category: danca, active: true },
    { name: 'Rodízio', category: gastronomia, active: true },
    { name: 'Festival', category: festa, active: true }
  ])
end