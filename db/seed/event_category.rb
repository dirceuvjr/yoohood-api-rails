require 'active_support/all'

module Seed
  evento = Event.first
  categorias = Category.where("category_id = 1") #Eventos gastronômicos
  EventCategory.create([
    { event: evento, category: categorias[0] },
    { event: evento, category: categorias[1] },
    { event: evento, category: categorias[2] }
  ])
end