module Seed
  User.create([
    {
      name: 'Administrador',
      email: 'admin@yoohood.fun',
      city: 'Curitiba',
      state: 'Paraná',
      country: 'Brasil'
    },
    {
      name: 'Convidado',
      email: 'guest@yoohood.fun',
      city: 'Curitiba',
      state: 'Paraná',
      country: 'Brasil'
    }
  ])
end