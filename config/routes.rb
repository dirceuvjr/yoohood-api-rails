Rails.application.routes.draw do
  get '/page/sponsored', to: 'events#sponsored'
  get '/page/all', to: 'events#all'
  get '/page/attendance/:id', to: 'events#get_total_accepted_invites'
  post '/user/referral', to: 'users#accept_invite'
  get '/user/inviter', to: 'users#get_inviters'
  
  resources :events, :path => 'page'
  resources :categories
  resources :users, :path => 'user'

  post '/auth/login', to: 'users#login'
  get '/user/:id/pages', to: 'users#get_user_events'
  post '/user/:id/page', to: 'users#create_user_events'
  delete '/user/:id/page/:event_id', to: 'users#delete_user_event'
  get '/user/:id/referral', to: 'users#get_referral'
  get '/user/:id/referral/report', to: 'users#get_referral_report'
  get '/user/:facebook_id/facebook', to: 'users#show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
